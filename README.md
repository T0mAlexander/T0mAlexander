## 🧑🏻‍💻 Sobre mim

<div style="display: flex;">
  <a href="https://open.spotify.com/user/wintonmello5?si=b23086650e3b40a8">
    <img align="right" src="https://spotify-github-profile.vercel.app/api/view?uid=wintonmello5&cover_image=true&theme=default&show_offline=true&background_color=121212&interchange=true&bar_color_cover=false&bar_color=53b14f" />
  </a>

  <ul>
    <li>💼 Atualmente estou trabalhando solo numa ONG como voluntário conduzindo um projeto desde o UX/UI até o desenvolvimento da aplicação</li>
    <li>🌱 Tenho <b>2 anos de experiência</b> como Dev Fullstack, trabalhando em diversos projetos</li>
    <li>📚 Estou me tornando <a href="https://aws.amazon.com/pt/devops/what-is-devops/"><b>DevOps Engineer</b></a></li>
    <li>🎓 Estudei através da <b>Udemy</b>, <b>Rocketseat</b> e atualmente na <b>Alura</b></li>
    <li> 🇬🇧 Eu falo inglês britânico <i>(oi bruv, can you gimme that <s>bottle of water</s> bo'ohw'o'wo'er?)</i></li>
    <li>
      <details>
        <summary>🕵🏻 Curiosidades</summary>
        <ul>
          <br>
          <li> ✡️ Eu sou <a href="https://pt.wikipedia.org/wiki/Juda%C3%ADsmo"><b>Judeu</b></a></li>
          <li>🎧 A música que mais escutei este ano é a <a href="https://www.youtube.com/watch?v=RQ9_TKayu9s"><b>Cleanin' Out My Closet</b></a> do <b>Eminem</b>
          <li>🏎️ Meu esporte favorito é Fórmula 1</li>
          <li>Minha filosofia para inspiração são os <a href="https://senna.com/os-valores-do-ayrton-senna-seguem-vivos-em-cada-um-de-nos/"><b>Valores do Senna</b></a> 
          <li>📰 Às vezes, eu escrevo artigos no <a href="https://medium.com/"><b>Medium</b></a></li>
        </ul>
      </details>
    </li>
  </ul>
</div>

<div align="left">
  <a href="mailto:t0malexander@protonmail.com">
    <img height="30" style="border-radius: 6px" src="https://img.shields.io/badge/ProtonMail-8B89CC?style=for-the-badge&logo=protonmail&logoColor=white" />
  </a>
  <a href="https://linkedin.com/in/t0m-alexander" target="_blank">
    <img height="30" style="border-radius: 6px" src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" />
  </a>
  <img height="30" style="border-radius: 6px" src="https://komarev.com/ghpvc/?username=T0mAlexander&style=for-the-badge&color=brightgreen&label=Visitantes" />
</div>

## 🧠 Habilidades e ferramentas

<div>
  <table>
    <caption>
      <h3>Front-end & Back-end</h3>
    </caption>
    <tr>
      <td align="center">
        <img src="https://skillicons.dev/icons?i=react" width="65px"/>
        <sub>
          <b>
            <h3>React</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="60px" src="https://res.cloudinary.com/tommello/image/upload/v1687705710/Github/Profile%20Markdown/iconizer-react_native_uozofa_bx0pjn.svg" title="React Native"/>
        <sub>
          <b>
            <h3>React Native</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=nextjs" title="Next.js"/>
        <sub>
          <b>
            <h3>Next.js</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=ts" title="Typescript"/>
        <sub>
          <b>
            <h3>Typescript</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=styledcomponents" title="Styled Components"/>
        <sub>
          <b>
            <h4>Styled Components</h4>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=nodejs" title="Node.js"/>
        <sub>
          <b>
            <h3>Node.js</h3>
          </b>
        </sub>
      </td>
      </tr>
      <tr>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=nestjs" title="Nest.js"/>
        <sub>
          <b>
            <h3>Nest.js</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=golang" title="Golang"/>
        <sub>
          <b>
            <h3>Golang</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=prisma" title="Prisma"/>
        <sub>
          <b>
            <h3>Prisma</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=jest" title="Jest"/>
        <sub>
          <b>
            <h3>Jest</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=postgresql" title="PostgreSQL"/>
        <sub>
          <b>
            <h3>PostgreSQL</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=figma" title="Figma"/>
        <sub>
          <b>
            <h3>Figma</h3>
          </b>
        </sub>
      </td>
    </tr>
  </table>
  <table>
    <caption>
      <h3>DevOps & Infraestrutura</h3>
    </caption>
    <tr>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=linux" title="Linux"/>
        <sub>
          <b>
            <h3>Linux</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=bash" title="Bash"/>
        <sub>
          <b>
            <h3>Bash</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=nginx" title="NGINX"/>
        <sub>
          <b>
            <h3>NGINX</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=docker" title="Docker"/>
        <sub>
          <b>
            <h3>Docker</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=kubernetes" title="Kubernetes"/>
        <sub>
          <b>
            <h3>Kubernetes</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://res.cloudinary.com/tommello/image/upload/v1687709304/Github/Profile%20Markdown/iconizer-terraform-original_vl0ivu.svg" title="Terraform"/>
        <sub>
          <b>
            <h3>Terraform</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=jenkins" title="Jenkins"/>
        <sub>
          <b>
            <h3>Jenkins</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=ansible" title="Ansible"/>
        <sub>
          <b>
            <h3>Ansible</h3>
          </b>
        </sub>
      </td>
    </tr>
  </table>
</div>

## 🎯 Aprendizados e interesses

<div>
  <table>
    <tr>
      <td align="center">
        <img width="65px" src="https://res.cloudinary.com/tommello/image/upload/v1687706787/Github/Profile%20Markdown/iconizer-argo-icon-color_vcyu6o.svg" title="ArgoCD" />
        <sub>
          <b>
            <h3>ArgoCD</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://res.cloudinary.com/tommello/image/upload/v1687708630/Github/Profile%20Markdown/istio_qewlid.svg" title="Istio" />
        <sub>
          <b>
            <h3>Istio</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://res.cloudinary.com/tommello/image/upload/v1687708940/Github/Profile%20Markdown/iconizer-hashicorp-vault_mohwti_lysrlm.svg" title="Vault" />
        <sub>
          <b>
            <h3>Vault</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=prometheus" title="Prometheus" />
        <sub>
          <b>
            <h3>Prometheus</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://res.cloudinary.com/tommello/image/upload/v1687734571/Github/Profile%20Markdown/Jaeger-Tracing_vxm0ug.svg" title="Jaeger" />
        <sub>
          <b>
            <h3>Jaeger</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=grafana" title="Grafana" />
        <sub>
          <b>
            <h3>Grafana</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=kafka" title="Kafka" />
        <sub>
          <b>
            <h3>Kafka</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=dynamodb" title="DynamoDB" />
        <sub>
          <b>
            <h3>DynamoDB</h3>
          </b>
        </sub>
      </td>
      <td align="center">
        <img width="65px" src="https://skillicons.dev/icons?i=aws" title="AWS" />
        <sub>
          <b>
            <h3>AWS</h3>
          </b>
        </sub>
      </td>
    </tr>
  </table>
</div>


## 📊 Estatísticas e atividade

<img align="center" src="https://github-readme-activity-graph.vercel.app/graph?username=T0mAlexander&theme=react&radius=10&custom_title=Contribuições%20de%20Código" />

<div align="center">
  <img align="left" style="margin-top: 100px;" src="https://github-readme-stats.vercel.app/api/top-langs/?username=T0mAlexander&theme=apprentice&locale=pt-BR&hide=html,css,ejs,hcl" />

  <img align="center" height="200" src="https://streak-stats.demolab.com?user=T0mAlexander&theme=transparent&date_format=%5BY%20%5DM%20j&mode=weekly&background=262626&ring=069DC2&fire=FFFFAF&currStreakNum=BCBCBC&sideNums=BCBCBC&sideLabels=BCBCBC&dates=BCBCBC&currStreakLabel=069DC2&locale=pt_BR" />
  <img align="center" height="200" src="https://github-readme-stats.vercel.app/api?username=T0mAlexander&show_icons=true&theme=apprentice&custom_title=Estatísticas&ring_color=00B4E0&line_height=28&card_width=200" />
  <img align="center" src="https://github-readme-stats.vercel.app/api/wakatime?username=TomAlexander&layout=compact&custom_title=Tempo%20de%20C%C3%B3digo&theme=apprentice" />
</div>
